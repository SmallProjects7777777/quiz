package org.example;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

        Quiz quiz = new Quiz("Quiz.json");
        quiz.play();
    }
}